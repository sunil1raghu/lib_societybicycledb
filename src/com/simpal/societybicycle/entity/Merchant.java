/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SunilRaghuvanshi
 */
@Entity
@Table(name = "merchant", catalog = "society_cycle", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Merchant.findAll", query = "SELECT m FROM Merchant m"),
    @NamedQuery(name = "Merchant.findById", query = "SELECT m FROM Merchant m WHERE m.id = :id"),
    @NamedQuery(name = "Merchant.findByMerchantCode", query = "SELECT m FROM Merchant m WHERE m.merchantCode = :merchantCode"),
    @NamedQuery(name = "Merchant.findByMerchantName", query = "SELECT m FROM Merchant m WHERE m.merchantName = :merchantName"),
    @NamedQuery(name = "Merchant.findByMerchantType", query = "SELECT m FROM Merchant m WHERE m.merchantType = :merchantType"),
    @NamedQuery(name = "Merchant.findByMobile", query = "SELECT m FROM Merchant m WHERE m.mobile = :mobile"),
    @NamedQuery(name = "Merchant.findByEmail", query = "SELECT m FROM Merchant m WHERE m.email = :email"),
    @NamedQuery(name = "Merchant.findByAddress", query = "SELECT m FROM Merchant m WHERE m.address = :address"),
    @NamedQuery(name = "Merchant.findByLatitude", query = "SELECT m FROM Merchant m WHERE m.latitude = :latitude"),
    @NamedQuery(name = "Merchant.findByLongitude", query = "SELECT m FROM Merchant m WHERE m.longitude = :longitude"),
    @NamedQuery(name = "Merchant.findByStatus", query = "SELECT m FROM Merchant m WHERE m.status = :status")})
public class Merchant implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "merchant_code", length = 12)
    private String merchantCode;
    @Column(name = "merchant_name", length = 40)
    private String merchantName;
    @Column(name = "merchant_type", length = 20)
    private String merchantType;
    @Column(name = "mobile", length = 10)
    private String mobile;
    @Column(name = "email", length = 50)
    private String email;
    @Column(name = "address", length = 200)
    private String address;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "latitude", precision = 17, scale = 17)
    private Double latitude;
    @Column(name = "longitude", precision = 17, scale = 17)
    private Double longitude;
    @Column(name = "status", length = 2)
    private String status;
    @OneToMany(mappedBy = "merchantId", fetch = FetchType.EAGER)
    private List<Bicycle> bicycleList;
    @OneToMany(mappedBy = "merchantId", fetch = FetchType.EAGER)
    private List<Device> deviceList;
    @OneToMany(mappedBy = "merchantId", fetch = FetchType.EAGER)
    private List<Operator> operatorList;

    public Merchant() {
    }

    public Merchant(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMerchantCode() {
        return merchantCode;
    }

    public void setMerchantCode(String merchantCode) {
        this.merchantCode = merchantCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public List<Bicycle> getBicycleList() {
        return bicycleList;
    }

    public void setBicycleList(List<Bicycle> bicycleList) {
        this.bicycleList = bicycleList;
    }

    @XmlTransient
    public List<Device> getDeviceList() {
        return deviceList;
    }

    public void setDeviceList(List<Device> deviceList) {
        this.deviceList = deviceList;
    }

    @XmlTransient
    public List<Operator> getOperatorList() {
        return operatorList;
    }

    public void setOperatorList(List<Operator> operatorList) {
        this.operatorList = operatorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Merchant)) {
            return false;
        }
        Merchant other = (Merchant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.simpal.societybicycle.entity.Merchant[ id=" + id + " ]";
    }
    
}
