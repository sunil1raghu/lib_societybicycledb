/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SunilRaghuvanshi
 */
@Entity
@Table(name = "summary", catalog = "society_cycle", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Summary.findAll", query = "SELECT s FROM Summary s"),
    @NamedQuery(name = "Summary.findByCustName", query = "SELECT s FROM Summary s WHERE s.custName = :custName"),
    @NamedQuery(name = "Summary.findByOprName", query = "SELECT s FROM Summary s WHERE s.oprName = :oprName"),
    @NamedQuery(name = "Summary.findByMerchantId", query = "SELECT s FROM Summary s WHERE s.merchantId = :merchantId"),
    @NamedQuery(name = "Summary.findByOprZsn", query = "SELECT s FROM Summary s WHERE s.oprZsn = :oprZsn"),
    @NamedQuery(name = "Summary.findByCycleNo", query = "SELECT s FROM Summary s WHERE s.cycleNo = :cycleNo"),
    @NamedQuery(name = "Summary.findByIssueTime", query = "SELECT s FROM Summary s WHERE s.issueTime = :issueTime"),
    @NamedQuery(name = "Summary.findByDepositTime", query = "SELECT s FROM Summary s WHERE s.depositTime = :depositTime"),
    @NamedQuery(name = "Summary.findByCustomerZsn", query = "SELECT s FROM Summary s WHERE s.customerZsn = :customerZsn")})
public class Summary implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    @Column(name = "cust_name", length = 40)
    private String custName;
    @Column(name = "opr_name", length = 40)
    private String oprName;
    @Column(name = "merchant_id")
    private Integer merchantId;
    @Column(name = "opr_zsn", length = 9)
    private String oprZsn;
    @Column(name = "cycle_no", length = 15)
    private String cycleNo;
    
    @Id
    @Column(name = "issue_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueTime;
    @Column(name = "deposit_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date depositTime;
    @Column(name = "customer_zsn", length = 9)
    private String customerZsn;

    public Summary() {
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String oprName) {
        this.oprName = oprName;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getOprZsn() {
        return oprZsn;
    }

    public void setOprZsn(String oprZsn) {
        this.oprZsn = oprZsn;
    }

    public String getCycleNo() {
        return cycleNo;
    }

    public void setCycleNo(String cycleNo) {
        this.cycleNo = cycleNo;
    }

    public Date getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(Date issueTime) {
        this.issueTime = issueTime;
    }

    public Date getDepositTime() {
        return depositTime;
    }

    public void setDepositTime(Date depositTime) {
        this.depositTime = depositTime;
    }

    public String getCustomerZsn() {
        return customerZsn;
    }

    public void setCustomerZsn(String customerZsn) {
        this.customerZsn = customerZsn;
    }
    
}
