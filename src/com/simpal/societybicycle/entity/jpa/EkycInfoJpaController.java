/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.simpal.societybicycle.entity.Customer;
import com.simpal.societybicycle.entity.EkycInfo;
import com.simpal.societybicycle.entity.jpa.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author SunilRaghuvanshi
 */
public class EkycInfoJpaController implements Serializable {

    public EkycInfoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(final EkycInfo ekycInfo) {
        if (ekycInfo.getCustomerList() == null) {
            ekycInfo.setCustomerList(new ArrayList<Customer>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Customer> attachedCustomerList = new ArrayList<Customer>();
            for (Customer customerListCustomerToAttach : ekycInfo.getCustomerList()) {
                customerListCustomerToAttach = em.getReference(customerListCustomerToAttach.getClass(), customerListCustomerToAttach.getId());
                attachedCustomerList.add(customerListCustomerToAttach);
            }
            ekycInfo.setCustomerList(attachedCustomerList);
            em.persist(ekycInfo);
            for (Customer customerListCustomer : ekycInfo.getCustomerList()) {
                EkycInfo oldEkycIdOfCustomerListCustomer = customerListCustomer.getEkycId();
                customerListCustomer.setEkycId(ekycInfo);
                customerListCustomer = em.merge(customerListCustomer);
                if (oldEkycIdOfCustomerListCustomer != null) {
                    oldEkycIdOfCustomerListCustomer.getCustomerList().remove(customerListCustomer);
                    oldEkycIdOfCustomerListCustomer = em.merge(oldEkycIdOfCustomerListCustomer);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(EkycInfo ekycInfo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EkycInfo persistentEkycInfo = em.find(EkycInfo.class, ekycInfo.getEkycId());
            List<Customer> customerListOld = persistentEkycInfo.getCustomerList();
            List<Customer> customerListNew = ekycInfo.getCustomerList();
            List<Customer> attachedCustomerListNew = new ArrayList<Customer>();
            for (Customer customerListNewCustomerToAttach : customerListNew) {
                customerListNewCustomerToAttach = em.getReference(customerListNewCustomerToAttach.getClass(), customerListNewCustomerToAttach.getId());
                attachedCustomerListNew.add(customerListNewCustomerToAttach);
            }
            customerListNew = attachedCustomerListNew;
            ekycInfo.setCustomerList(customerListNew);
            ekycInfo = em.merge(ekycInfo);
            for (Customer customerListOldCustomer : customerListOld) {
                if (!customerListNew.contains(customerListOldCustomer)) {
                    customerListOldCustomer.setEkycId(null);
                    customerListOldCustomer = em.merge(customerListOldCustomer);
                }
            }
            for (Customer customerListNewCustomer : customerListNew) {
                if (!customerListOld.contains(customerListNewCustomer)) {
                    EkycInfo oldEkycIdOfCustomerListNewCustomer = customerListNewCustomer.getEkycId();
                    customerListNewCustomer.setEkycId(ekycInfo);
                    customerListNewCustomer = em.merge(customerListNewCustomer);
                    if (oldEkycIdOfCustomerListNewCustomer != null && !oldEkycIdOfCustomerListNewCustomer.equals(ekycInfo)) {
                        oldEkycIdOfCustomerListNewCustomer.getCustomerList().remove(customerListNewCustomer);
                        oldEkycIdOfCustomerListNewCustomer = em.merge(oldEkycIdOfCustomerListNewCustomer);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ekycInfo.getEkycId();
                if (findEkycInfo(id) == null) {
                    throw new NonexistentEntityException("The ekycInfo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EkycInfo ekycInfo;
            try {
                ekycInfo = em.getReference(EkycInfo.class, id);
                ekycInfo.getEkycId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The ekycInfo with id " + id + " no longer exists.", enfe);
            }
            List<Customer> customerList = ekycInfo.getCustomerList();
            for (Customer customerListCustomer : customerList) {
                customerListCustomer.setEkycId(null);
                customerListCustomer = em.merge(customerListCustomer);
            }
            em.remove(ekycInfo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<EkycInfo> findEkycInfoEntities() {
        return findEkycInfoEntities(true, -1, -1);
    }

    public List<EkycInfo> findEkycInfoEntities(int maxResults, int firstResult) {
        return findEkycInfoEntities(false, maxResults, firstResult);
    }

    private List<EkycInfo> findEkycInfoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(EkycInfo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public EkycInfo findEkycInfo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(EkycInfo.class, id);
        } finally {
            em.close();
        }
    }

    public int getEkycInfoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<EkycInfo> rt = cq.from(EkycInfo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
