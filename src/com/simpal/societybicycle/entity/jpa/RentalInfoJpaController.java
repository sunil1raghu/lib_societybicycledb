/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.simpal.societybicycle.entity.Device;
import com.simpal.societybicycle.entity.Device_;
import com.simpal.societybicycle.entity.Operator;
import com.simpal.societybicycle.entity.RentalInfo;
import com.simpal.societybicycle.entity.RentalInfo_;
import com.simpal.societybicycle.entity.jpa.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author SunilRaghuvanshi
 */
public class RentalInfoJpaController implements Serializable {

    public RentalInfoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(RentalInfo rentalInfo) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Device issueDeviceId = rentalInfo.getIssueDeviceId();
            if (issueDeviceId != null) {
                issueDeviceId = em.getReference(issueDeviceId.getClass(), issueDeviceId.getId());
                rentalInfo.setIssueDeviceId(issueDeviceId);
            }
            Device depositDeviceId = rentalInfo.getDepositDeviceId();
            if (depositDeviceId != null) {
                depositDeviceId = em.getReference(depositDeviceId.getClass(), depositDeviceId.getId());
                rentalInfo.setDepositDeviceId(depositDeviceId);
            }
            em.persist(rentalInfo);
            if (issueDeviceId != null) {
                issueDeviceId.getRentalInfoList().add(rentalInfo);
                issueDeviceId = em.merge(issueDeviceId);
            }
            if (depositDeviceId != null) {
                depositDeviceId.getRentalInfoList().add(rentalInfo);
                depositDeviceId = em.merge(depositDeviceId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RentalInfo rentalInfo) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RentalInfo persistentRentalInfo = em.find(RentalInfo.class, rentalInfo.getId());
            Device issueDeviceIdOld = persistentRentalInfo.getIssueDeviceId();
            Device issueDeviceIdNew = rentalInfo.getIssueDeviceId();
            Device depositDeviceIdOld = persistentRentalInfo.getDepositDeviceId();
            Device depositDeviceIdNew = rentalInfo.getDepositDeviceId();
            if (issueDeviceIdNew != null) {
                issueDeviceIdNew = em.getReference(issueDeviceIdNew.getClass(), issueDeviceIdNew.getId());
                rentalInfo.setIssueDeviceId(issueDeviceIdNew);
            }
            if (depositDeviceIdNew != null) {
                depositDeviceIdNew = em.getReference(depositDeviceIdNew.getClass(), depositDeviceIdNew.getId());
                rentalInfo.setDepositDeviceId(depositDeviceIdNew);
            }
            rentalInfo = em.merge(rentalInfo);
            if (issueDeviceIdOld != null && !issueDeviceIdOld.equals(issueDeviceIdNew)) {
                issueDeviceIdOld.getRentalInfoList().remove(rentalInfo);
                issueDeviceIdOld = em.merge(issueDeviceIdOld);
            }
            if (issueDeviceIdNew != null && !issueDeviceIdNew.equals(issueDeviceIdOld)) {
                issueDeviceIdNew.getRentalInfoList().add(rentalInfo);
                issueDeviceIdNew = em.merge(issueDeviceIdNew);
            }
            if (depositDeviceIdOld != null && !depositDeviceIdOld.equals(depositDeviceIdNew)) {
                depositDeviceIdOld.getRentalInfoList().remove(rentalInfo);
                depositDeviceIdOld = em.merge(depositDeviceIdOld);
            }
            if (depositDeviceIdNew != null && !depositDeviceIdNew.equals(depositDeviceIdOld)) {
                depositDeviceIdNew.getRentalInfoList().add(rentalInfo);
                depositDeviceIdNew = em.merge(depositDeviceIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = rentalInfo.getId();
                if (findRentalInfo(id) == null) {
                    throw new NonexistentEntityException("The rentalInfo with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RentalInfo rentalInfo;
            try {
                rentalInfo = em.getReference(RentalInfo.class, id);
                rentalInfo.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rentalInfo with id " + id + " no longer exists.", enfe);
            }
            Device issueDeviceId = rentalInfo.getIssueDeviceId();
            if (issueDeviceId != null) {
                issueDeviceId.getRentalInfoList().remove(rentalInfo);
                issueDeviceId = em.merge(issueDeviceId);
            }
            Device depositDeviceId = rentalInfo.getDepositDeviceId();
            if (depositDeviceId != null) {
                depositDeviceId.getRentalInfoList().remove(rentalInfo);
                depositDeviceId = em.merge(depositDeviceId);
            }
            em.remove(rentalInfo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public RentalInfo findRentalInfoByCycleNo(String cycleNo) throws Exception {
        EntityManager em = getEntityManager();
        Operator op=null;
        try {
            
            CriteriaBuilder builder=em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<RentalInfo> rt = cq.from(RentalInfo.class);
            cq.where(builder.equal(rt.get(RentalInfo_.cycleNo), cycleNo));
            cq.orderBy(builder.desc(rt.get(RentalInfo_.issueTime)));
            //TODO - Latest Issuence information should be shown
            
//            cq.where(builder.equal(rt, rt))
            List<RentalInfo> oprlst=em.createQuery(cq).getResultList();
            if(oprlst.size()>0)
                return oprlst.get(0);
            else
                return null;
            
            
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
     public RentalInfo findRentalInfoByCycleNoAndCustomerZsn(String zsn,String cycleNo) throws Exception {
        EntityManager em = getEntityManager();
        Operator op=null;
        try {
            
            CriteriaBuilder builder=em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<RentalInfo> rt = cq.from(RentalInfo.class);
            cq.where(builder.equal(rt.get(RentalInfo_.customerZsn), zsn));
            cq.where(builder.equal(rt.get(RentalInfo_.cycleNo), cycleNo));
            cq.orderBy(builder.desc(rt.get(RentalInfo_.issueTime)));
            //TODO - Latest Issuence information should be shown
            
//            cq.where(builder.equal(rt, rt))
            List<RentalInfo> oprlst=em.createQuery(cq).getResultList();
            if(oprlst.size()>0)
                return oprlst.get(0);
            else
                return null;
            
            
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RentalInfo> findRentalInfoEntities() {
        return findRentalInfoEntities(true, -1, -1);
    }

    public List<RentalInfo> findRentalInfoEntities(int maxResults, int firstResult) {
        return findRentalInfoEntities(false, maxResults, firstResult);
    }

    private List<RentalInfo> findRentalInfoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RentalInfo.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RentalInfo findRentalInfo(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RentalInfo.class, id);
        } finally {
            em.close();
        }
    }

    public int getRentalInfoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RentalInfo> rt = cq.from(RentalInfo.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
