/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.simpal.societybicycle.entity.Merchant;
import com.simpal.societybicycle.entity.Operator;
import com.simpal.societybicycle.entity.Operator_;
import com.simpal.societybicycle.entity.jpa.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author SunilRaghuvanshi
 */
public class OperatorJpaController implements Serializable {

    public OperatorJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Operator operator) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchant merchantId = operator.getMerchantId();
            if (merchantId != null) {
                merchantId = em.getReference(merchantId.getClass(), merchantId.getId());
                operator.setMerchantId(merchantId);
            }
            em.persist(operator);
            if (merchantId != null) {
                merchantId.getOperatorList().add(operator);
                merchantId = em.merge(merchantId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    public void update(Operator operator) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchant merchantId = operator.getMerchantId();
            if (merchantId != null) {
                merchantId = em.getReference(merchantId.getClass(), merchantId.getId());
                operator.setMerchantId(merchantId);
            }
            em.merge(operator);
            if (merchantId != null) {
                merchantId.getOperatorList().add(operator);
                merchantId = em.merge(merchantId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    

    public Operator findOperatorByZsn(String zsn) throws Exception {
        EntityManager em = getEntityManager();
        Operator op=null;
        try {
  
            
            CriteriaBuilder builder=em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<Operator> rt = cq.from(Operator.class);
            cq.where(builder.equal(rt.get(Operator_.zsn), zsn));
            List<Operator> oprlst=em.createQuery(cq).getResultList();
            if(oprlst.size()>0)
                return oprlst.get(0);
            else
                return null;
            
            
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Operator operator;
            try {
                operator = em.getReference(Operator.class, id);
                operator.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The operator with id " + id + " no longer exists.", enfe);
            }
            Merchant merchantId = operator.getMerchantId();
            if (merchantId != null) {
                merchantId.getOperatorList().remove(operator);
                merchantId = em.merge(merchantId);
            }
            em.remove(operator);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Operator> findOperatorEntities() {
        return findOperatorEntities(true, -1, -1);
    }

    public List<Operator> findOperatorEntities(int maxResults, int firstResult) {
        return findOperatorEntities(false, maxResults, firstResult);
    }

    private List<Operator> findOperatorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Operator.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Operator findOperator(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Operator.class, id);
        } finally {
            em.close();
        }
    }

    public int getOperatorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Operator> rt = cq.from(Operator.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
