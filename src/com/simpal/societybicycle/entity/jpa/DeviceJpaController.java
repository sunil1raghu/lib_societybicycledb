/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import com.simpal.societybicycle.entity.Device;
import com.simpal.societybicycle.entity.Device_;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.simpal.societybicycle.entity.Merchant;
import com.simpal.societybicycle.entity.Operator;
import com.simpal.societybicycle.entity.Operator_;
import com.simpal.societybicycle.entity.RentalInfo;
import com.simpal.societybicycle.entity.jpa.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author SunilRaghuvanshi
 */
public class DeviceJpaController implements Serializable {

    public DeviceJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Device device) {
        if (device.getRentalInfoList() == null) {
            device.setRentalInfoList(new ArrayList<RentalInfo>());
        }
        if (device.getRentalInfoList1() == null) {
            device.setRentalInfoList1(new ArrayList<RentalInfo>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchant merchantId = device.getMerchantId();
            if (merchantId != null) {
                merchantId = em.getReference(merchantId.getClass(), merchantId.getId());
                device.setMerchantId(merchantId);
            }
            List<RentalInfo> attachedRentalInfoList = new ArrayList<RentalInfo>();
            for (RentalInfo rentalInfoListRentalInfoToAttach : device.getRentalInfoList()) {
                rentalInfoListRentalInfoToAttach = em.getReference(rentalInfoListRentalInfoToAttach.getClass(), rentalInfoListRentalInfoToAttach.getId());
                attachedRentalInfoList.add(rentalInfoListRentalInfoToAttach);
            }
            device.setRentalInfoList(attachedRentalInfoList);
            List<RentalInfo> attachedRentalInfoList1 = new ArrayList<RentalInfo>();
            for (RentalInfo rentalInfoList1RentalInfoToAttach : device.getRentalInfoList1()) {
                rentalInfoList1RentalInfoToAttach = em.getReference(rentalInfoList1RentalInfoToAttach.getClass(), rentalInfoList1RentalInfoToAttach.getId());
                attachedRentalInfoList1.add(rentalInfoList1RentalInfoToAttach);
            }
            device.setRentalInfoList1(attachedRentalInfoList1);
            em.persist(device);
            if (merchantId != null) {
                merchantId.getDeviceList().add(device);
                merchantId = em.merge(merchantId);
            }
            for (RentalInfo rentalInfoListRentalInfo : device.getRentalInfoList()) {
                Device oldIssueDeviceIdOfRentalInfoListRentalInfo = rentalInfoListRentalInfo.getIssueDeviceId();
                rentalInfoListRentalInfo.setIssueDeviceId(device);
                rentalInfoListRentalInfo = em.merge(rentalInfoListRentalInfo);
                if (oldIssueDeviceIdOfRentalInfoListRentalInfo != null) {
                    oldIssueDeviceIdOfRentalInfoListRentalInfo.getRentalInfoList().remove(rentalInfoListRentalInfo);
                    oldIssueDeviceIdOfRentalInfoListRentalInfo = em.merge(oldIssueDeviceIdOfRentalInfoListRentalInfo);
                }
            }
            for (RentalInfo rentalInfoList1RentalInfo : device.getRentalInfoList1()) {
                Device oldDepositDeviceIdOfRentalInfoList1RentalInfo = rentalInfoList1RentalInfo.getDepositDeviceId();
                rentalInfoList1RentalInfo.setDepositDeviceId(device);
                rentalInfoList1RentalInfo = em.merge(rentalInfoList1RentalInfo);
                if (oldDepositDeviceIdOfRentalInfoList1RentalInfo != null) {
                    oldDepositDeviceIdOfRentalInfoList1RentalInfo.getRentalInfoList1().remove(rentalInfoList1RentalInfo);
                    oldDepositDeviceIdOfRentalInfoList1RentalInfo = em.merge(oldDepositDeviceIdOfRentalInfoList1RentalInfo);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Device device) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Device persistentDevice = em.find(Device.class, device.getId());
            Merchant merchantIdOld = persistentDevice.getMerchantId();
            Merchant merchantIdNew = device.getMerchantId();
            List<RentalInfo> rentalInfoListOld = persistentDevice.getRentalInfoList();
            List<RentalInfo> rentalInfoListNew = device.getRentalInfoList();
            List<RentalInfo> rentalInfoList1Old = persistentDevice.getRentalInfoList1();
            List<RentalInfo> rentalInfoList1New = device.getRentalInfoList1();
            if (merchantIdNew != null) {
                merchantIdNew = em.getReference(merchantIdNew.getClass(), merchantIdNew.getId());
                device.setMerchantId(merchantIdNew);
            }
            List<RentalInfo> attachedRentalInfoListNew = new ArrayList<RentalInfo>();
            for (RentalInfo rentalInfoListNewRentalInfoToAttach : rentalInfoListNew) {
                rentalInfoListNewRentalInfoToAttach = em.getReference(rentalInfoListNewRentalInfoToAttach.getClass(), rentalInfoListNewRentalInfoToAttach.getId());
                attachedRentalInfoListNew.add(rentalInfoListNewRentalInfoToAttach);
            }
            rentalInfoListNew = attachedRentalInfoListNew;
            device.setRentalInfoList(rentalInfoListNew);
            List<RentalInfo> attachedRentalInfoList1New = new ArrayList<RentalInfo>();
            for (RentalInfo rentalInfoList1NewRentalInfoToAttach : rentalInfoList1New) {
                rentalInfoList1NewRentalInfoToAttach = em.getReference(rentalInfoList1NewRentalInfoToAttach.getClass(), rentalInfoList1NewRentalInfoToAttach.getId());
                attachedRentalInfoList1New.add(rentalInfoList1NewRentalInfoToAttach);
            }
            rentalInfoList1New = attachedRentalInfoList1New;
            device.setRentalInfoList1(rentalInfoList1New);
            device = em.merge(device);
            if (merchantIdOld != null && !merchantIdOld.equals(merchantIdNew)) {
                merchantIdOld.getDeviceList().remove(device);
                merchantIdOld = em.merge(merchantIdOld);
            }
            if (merchantIdNew != null && !merchantIdNew.equals(merchantIdOld)) {
                merchantIdNew.getDeviceList().add(device);
                merchantIdNew = em.merge(merchantIdNew);
            }
            for (RentalInfo rentalInfoListOldRentalInfo : rentalInfoListOld) {
                if (!rentalInfoListNew.contains(rentalInfoListOldRentalInfo)) {
                    rentalInfoListOldRentalInfo.setIssueDeviceId(null);
                    rentalInfoListOldRentalInfo = em.merge(rentalInfoListOldRentalInfo);
                }
            }
            for (RentalInfo rentalInfoListNewRentalInfo : rentalInfoListNew) {
                if (!rentalInfoListOld.contains(rentalInfoListNewRentalInfo)) {
                    Device oldIssueDeviceIdOfRentalInfoListNewRentalInfo = rentalInfoListNewRentalInfo.getIssueDeviceId();
                    rentalInfoListNewRentalInfo.setIssueDeviceId(device);
                    rentalInfoListNewRentalInfo = em.merge(rentalInfoListNewRentalInfo);
                    if (oldIssueDeviceIdOfRentalInfoListNewRentalInfo != null && !oldIssueDeviceIdOfRentalInfoListNewRentalInfo.equals(device)) {
                        oldIssueDeviceIdOfRentalInfoListNewRentalInfo.getRentalInfoList().remove(rentalInfoListNewRentalInfo);
                        oldIssueDeviceIdOfRentalInfoListNewRentalInfo = em.merge(oldIssueDeviceIdOfRentalInfoListNewRentalInfo);
                    }
                }
            }
            for (RentalInfo rentalInfoList1OldRentalInfo : rentalInfoList1Old) {
                if (!rentalInfoList1New.contains(rentalInfoList1OldRentalInfo)) {
                    rentalInfoList1OldRentalInfo.setDepositDeviceId(null);
                    rentalInfoList1OldRentalInfo = em.merge(rentalInfoList1OldRentalInfo);
                }
            }
            for (RentalInfo rentalInfoList1NewRentalInfo : rentalInfoList1New) {
                if (!rentalInfoList1Old.contains(rentalInfoList1NewRentalInfo)) {
                    Device oldDepositDeviceIdOfRentalInfoList1NewRentalInfo = rentalInfoList1NewRentalInfo.getDepositDeviceId();
                    rentalInfoList1NewRentalInfo.setDepositDeviceId(device);
                    rentalInfoList1NewRentalInfo = em.merge(rentalInfoList1NewRentalInfo);
                    if (oldDepositDeviceIdOfRentalInfoList1NewRentalInfo != null && !oldDepositDeviceIdOfRentalInfoList1NewRentalInfo.equals(device)) {
                        oldDepositDeviceIdOfRentalInfoList1NewRentalInfo.getRentalInfoList1().remove(rentalInfoList1NewRentalInfo);
                        oldDepositDeviceIdOfRentalInfoList1NewRentalInfo = em.merge(oldDepositDeviceIdOfRentalInfoList1NewRentalInfo);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = device.getId();
                if (findDevice(id) == null) {
                    throw new NonexistentEntityException("The device with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Device device;
            try {
                device = em.getReference(Device.class, id);
                device.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The device with id " + id + " no longer exists.", enfe);
            }
            Merchant merchantId = device.getMerchantId();
            if (merchantId != null) {
                merchantId.getDeviceList().remove(device);
                merchantId = em.merge(merchantId);
            }
            List<RentalInfo> rentalInfoList = device.getRentalInfoList();
            for (RentalInfo rentalInfoListRentalInfo : rentalInfoList) {
                rentalInfoListRentalInfo.setIssueDeviceId(null);
                rentalInfoListRentalInfo = em.merge(rentalInfoListRentalInfo);
            }
            List<RentalInfo> rentalInfoList1 = device.getRentalInfoList1();
            for (RentalInfo rentalInfoList1RentalInfo : rentalInfoList1) {
                rentalInfoList1RentalInfo.setDepositDeviceId(null);
                rentalInfoList1RentalInfo = em.merge(rentalInfoList1RentalInfo);
            }
            em.remove(device);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Device> findDeviceEntities() {
        return findDeviceEntities(true, -1, -1);
    }

    public List<Device> findDeviceEntities(int maxResults, int firstResult) {
        return findDeviceEntities(false, maxResults, firstResult);
    }
    
    public Device findDeviceByImei(String imei) throws Exception {
        EntityManager em = getEntityManager();
        Operator op=null;
        try {
            
            CriteriaBuilder builder=em.getCriteriaBuilder();
            CriteriaQuery cq = builder.createQuery();
            Root<Device> rt = cq.from(Device.class);
            cq.where(builder.equal(rt.get(Device_.imei), imei));
            List<Device> oprlst=em.createQuery(cq).getResultList();
            if(oprlst.size()>0)
                return oprlst.get(0);
            else
                return null;
            
            
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private List<Device> findDeviceEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Device.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Device findDevice(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Device.class, id);
        } finally {
            em.close();
        }
    }

    public int getDeviceCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Device> rt = cq.from(Device.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
