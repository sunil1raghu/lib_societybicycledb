/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.simpal.societybicycle.entity.Bicycle;
import java.util.ArrayList;
import java.util.List;
import com.simpal.societybicycle.entity.Device;
import com.simpal.societybicycle.entity.Merchant;
import com.simpal.societybicycle.entity.Operator;
import com.simpal.societybicycle.entity.jpa.exceptions.NonexistentEntityException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author SunilRaghuvanshi
 */
public class MerchantJpaController implements Serializable {

    public MerchantJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Merchant merchant) {
        if (merchant.getBicycleList() == null) {
            merchant.setBicycleList(new ArrayList<Bicycle>());
        }
        if (merchant.getDeviceList() == null) {
            merchant.setDeviceList(new ArrayList<Device>());
        }
        if (merchant.getOperatorList() == null) {
            merchant.setOperatorList(new ArrayList<Operator>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Bicycle> attachedBicycleList = new ArrayList<Bicycle>();
            for (Bicycle bicycleListBicycleToAttach : merchant.getBicycleList()) {
                bicycleListBicycleToAttach = em.getReference(bicycleListBicycleToAttach.getClass(), bicycleListBicycleToAttach.getId());
                attachedBicycleList.add(bicycleListBicycleToAttach);
            }
            merchant.setBicycleList(attachedBicycleList);
            List<Device> attachedDeviceList = new ArrayList<Device>();
            for (Device deviceListDeviceToAttach : merchant.getDeviceList()) {
                deviceListDeviceToAttach = em.getReference(deviceListDeviceToAttach.getClass(), deviceListDeviceToAttach.getId());
                attachedDeviceList.add(deviceListDeviceToAttach);
            }
            merchant.setDeviceList(attachedDeviceList);
            List<Operator> attachedOperatorList = new ArrayList<Operator>();
            for (Operator operatorListOperatorToAttach : merchant.getOperatorList()) {
                operatorListOperatorToAttach = em.getReference(operatorListOperatorToAttach.getClass(), operatorListOperatorToAttach.getId());
                attachedOperatorList.add(operatorListOperatorToAttach);
            }
            merchant.setOperatorList(attachedOperatorList);
            em.persist(merchant);
            for (Bicycle bicycleListBicycle : merchant.getBicycleList()) {
                Merchant oldMerchantIdOfBicycleListBicycle = bicycleListBicycle.getMerchantId();
                bicycleListBicycle.setMerchantId(merchant);
                bicycleListBicycle = em.merge(bicycleListBicycle);
                if (oldMerchantIdOfBicycleListBicycle != null) {
                    oldMerchantIdOfBicycleListBicycle.getBicycleList().remove(bicycleListBicycle);
                    oldMerchantIdOfBicycleListBicycle = em.merge(oldMerchantIdOfBicycleListBicycle);
                }
            }
            for (Device deviceListDevice : merchant.getDeviceList()) {
                Merchant oldMerchantIdOfDeviceListDevice = deviceListDevice.getMerchantId();
                deviceListDevice.setMerchantId(merchant);
                deviceListDevice = em.merge(deviceListDevice);
                if (oldMerchantIdOfDeviceListDevice != null) {
                    oldMerchantIdOfDeviceListDevice.getDeviceList().remove(deviceListDevice);
                    oldMerchantIdOfDeviceListDevice = em.merge(oldMerchantIdOfDeviceListDevice);
                }
            }
            for (Operator operatorListOperator : merchant.getOperatorList()) {
                Merchant oldMerchantIdOfOperatorListOperator = operatorListOperator.getMerchantId();
                operatorListOperator.setMerchantId(merchant);
                operatorListOperator = em.merge(operatorListOperator);
                if (oldMerchantIdOfOperatorListOperator != null) {
                    oldMerchantIdOfOperatorListOperator.getOperatorList().remove(operatorListOperator);
                    oldMerchantIdOfOperatorListOperator = em.merge(oldMerchantIdOfOperatorListOperator);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Merchant merchant) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchant persistentMerchant = em.find(Merchant.class, merchant.getId());
            List<Bicycle> bicycleListOld = persistentMerchant.getBicycleList();
            List<Bicycle> bicycleListNew = merchant.getBicycleList();
            List<Device> deviceListOld = persistentMerchant.getDeviceList();
            List<Device> deviceListNew = merchant.getDeviceList();
            List<Operator> operatorListOld = persistentMerchant.getOperatorList();
            List<Operator> operatorListNew = merchant.getOperatorList();
            List<Bicycle> attachedBicycleListNew = new ArrayList<Bicycle>();
            for (Bicycle bicycleListNewBicycleToAttach : bicycleListNew) {
                bicycleListNewBicycleToAttach = em.getReference(bicycleListNewBicycleToAttach.getClass(), bicycleListNewBicycleToAttach.getId());
                attachedBicycleListNew.add(bicycleListNewBicycleToAttach);
            }
            bicycleListNew = attachedBicycleListNew;
            merchant.setBicycleList(bicycleListNew);
            List<Device> attachedDeviceListNew = new ArrayList<Device>();
            for (Device deviceListNewDeviceToAttach : deviceListNew) {
                deviceListNewDeviceToAttach = em.getReference(deviceListNewDeviceToAttach.getClass(), deviceListNewDeviceToAttach.getId());
                attachedDeviceListNew.add(deviceListNewDeviceToAttach);
            }
            deviceListNew = attachedDeviceListNew;
            merchant.setDeviceList(deviceListNew);
            List<Operator> attachedOperatorListNew = new ArrayList<Operator>();
            for (Operator operatorListNewOperatorToAttach : operatorListNew) {
                operatorListNewOperatorToAttach = em.getReference(operatorListNewOperatorToAttach.getClass(), operatorListNewOperatorToAttach.getId());
                attachedOperatorListNew.add(operatorListNewOperatorToAttach);
            }
            operatorListNew = attachedOperatorListNew;
            merchant.setOperatorList(operatorListNew);
            merchant = em.merge(merchant);
            for (Bicycle bicycleListOldBicycle : bicycleListOld) {
                if (!bicycleListNew.contains(bicycleListOldBicycle)) {
                    bicycleListOldBicycle.setMerchantId(null);
                    bicycleListOldBicycle = em.merge(bicycleListOldBicycle);
                }
            }
            for (Bicycle bicycleListNewBicycle : bicycleListNew) {
                if (!bicycleListOld.contains(bicycleListNewBicycle)) {
                    Merchant oldMerchantIdOfBicycleListNewBicycle = bicycleListNewBicycle.getMerchantId();
                    bicycleListNewBicycle.setMerchantId(merchant);
                    bicycleListNewBicycle = em.merge(bicycleListNewBicycle);
                    if (oldMerchantIdOfBicycleListNewBicycle != null && !oldMerchantIdOfBicycleListNewBicycle.equals(merchant)) {
                        oldMerchantIdOfBicycleListNewBicycle.getBicycleList().remove(bicycleListNewBicycle);
                        oldMerchantIdOfBicycleListNewBicycle = em.merge(oldMerchantIdOfBicycleListNewBicycle);
                    }
                }
            }
            for (Device deviceListOldDevice : deviceListOld) {
                if (!deviceListNew.contains(deviceListOldDevice)) {
                    deviceListOldDevice.setMerchantId(null);
                    deviceListOldDevice = em.merge(deviceListOldDevice);
                }
            }
            for (Device deviceListNewDevice : deviceListNew) {
                if (!deviceListOld.contains(deviceListNewDevice)) {
                    Merchant oldMerchantIdOfDeviceListNewDevice = deviceListNewDevice.getMerchantId();
                    deviceListNewDevice.setMerchantId(merchant);
                    deviceListNewDevice = em.merge(deviceListNewDevice);
                    if (oldMerchantIdOfDeviceListNewDevice != null && !oldMerchantIdOfDeviceListNewDevice.equals(merchant)) {
                        oldMerchantIdOfDeviceListNewDevice.getDeviceList().remove(deviceListNewDevice);
                        oldMerchantIdOfDeviceListNewDevice = em.merge(oldMerchantIdOfDeviceListNewDevice);
                    }
                }
            }
            for (Operator operatorListOldOperator : operatorListOld) {
                if (!operatorListNew.contains(operatorListOldOperator)) {
                    operatorListOldOperator.setMerchantId(null);
                    operatorListOldOperator = em.merge(operatorListOldOperator);
                }
            }
            for (Operator operatorListNewOperator : operatorListNew) {
                if (!operatorListOld.contains(operatorListNewOperator)) {
                    Merchant oldMerchantIdOfOperatorListNewOperator = operatorListNewOperator.getMerchantId();
                    operatorListNewOperator.setMerchantId(merchant);
                    operatorListNewOperator = em.merge(operatorListNewOperator);
                    if (oldMerchantIdOfOperatorListNewOperator != null && !oldMerchantIdOfOperatorListNewOperator.equals(merchant)) {
                        oldMerchantIdOfOperatorListNewOperator.getOperatorList().remove(operatorListNewOperator);
                        oldMerchantIdOfOperatorListNewOperator = em.merge(oldMerchantIdOfOperatorListNewOperator);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = merchant.getId();
                if (findMerchant(id) == null) {
                    throw new NonexistentEntityException("The merchant with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchant merchant;
            try {
                merchant = em.getReference(Merchant.class, id);
                merchant.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The merchant with id " + id + " no longer exists.", enfe);
            }
            List<Bicycle> bicycleList = merchant.getBicycleList();
            for (Bicycle bicycleListBicycle : bicycleList) {
                bicycleListBicycle.setMerchantId(null);
                bicycleListBicycle = em.merge(bicycleListBicycle);
            }
            List<Device> deviceList = merchant.getDeviceList();
            for (Device deviceListDevice : deviceList) {
                deviceListDevice.setMerchantId(null);
                deviceListDevice = em.merge(deviceListDevice);
            }
            List<Operator> operatorList = merchant.getOperatorList();
            for (Operator operatorListOperator : operatorList) {
                operatorListOperator.setMerchantId(null);
                operatorListOperator = em.merge(operatorListOperator);
            }
            em.remove(merchant);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Merchant> findMerchantEntities() {
        return findMerchantEntities(true, -1, -1);
    }

    public List<Merchant> findMerchantEntities(int maxResults, int firstResult) {
        return findMerchantEntities(false, maxResults, firstResult);
    }

    private List<Merchant> findMerchantEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Merchant.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Merchant findMerchant(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Merchant.class, id);
        } finally {
            em.close();
        }
    }

    public int getMerchantCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Merchant> rt = cq.from(Merchant.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
