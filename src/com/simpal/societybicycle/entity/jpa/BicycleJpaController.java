/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import com.simpal.societybicycle.entity.Bicycle;
import com.simpal.societybicycle.entity.Bicycle_;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.simpal.societybicycle.entity.Merchant;
import com.simpal.societybicycle.entity.Operator;
import static com.simpal.societybicycle.entity.Operator_.zsn;
import com.simpal.societybicycle.entity.Summary;
import com.simpal.societybicycle.entity.jpa.exceptions.NonexistentEntityException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;

/**
 *
 * @author SunilRaghuvanshi
 */
public class BicycleJpaController implements Serializable {

    public BicycleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Bicycle bicycle) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Merchant merchantId = bicycle.getMerchantId();
            if (merchantId != null) {
                merchantId = em.getReference(merchantId.getClass(), merchantId.getId());
                bicycle.setMerchantId(merchantId);
            }
            em.persist(bicycle);
            if (merchantId != null) {
                merchantId.getBicycleList().add(bicycle);
                merchantId = em.merge(merchantId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Bicycle bicycle) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bicycle persistentBicycle = em.find(Bicycle.class, bicycle.getId());
            Merchant merchantIdOld = persistentBicycle.getMerchantId();
            Merchant merchantIdNew = bicycle.getMerchantId();
            if (merchantIdNew != null) {
                merchantIdNew = em.getReference(merchantIdNew.getClass(), merchantIdNew.getId());
                bicycle.setMerchantId(merchantIdNew);
            }
            bicycle = em.merge(bicycle);
            if (merchantIdOld != null && !merchantIdOld.equals(merchantIdNew)) {
                merchantIdOld.getBicycleList().remove(bicycle);
                merchantIdOld = em.merge(merchantIdOld);
            }
            if (merchantIdNew != null && !merchantIdNew.equals(merchantIdOld)) {
                merchantIdNew.getBicycleList().add(bicycle);
                merchantIdNew = em.merge(merchantIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = bicycle.getId();
                if (findBicycle(id) == null) {
                    throw new NonexistentEntityException("The bicycle with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Bicycle bicycle;
            try {
                bicycle = em.getReference(Bicycle.class, id);
                bicycle.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The bicycle with id " + id + " no longer exists.", enfe);
            }
            Merchant merchantId = bicycle.getMerchantId();
            if (merchantId != null) {
                merchantId.getBicycleList().remove(bicycle);
                merchantId = em.merge(merchantId);
            }
            em.remove(bicycle);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Bicycle> findBicycleEntities() {
        return findBicycleEntities(true, -1, -1);
    }

    public List<Bicycle> findBicycleEntities(int maxResults, int firstResult) {
        return findBicycleEntities(false, maxResults, firstResult);
    }

    private List<Bicycle> findBicycleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Bicycle.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    
    public Bicycle findBicycleByNumber(String cycleNo) {
        EntityManager em = getEntityManager();
        try {
           Query query =em.createQuery("SELECT b FROM Bicycle b WHERE b.cycleNo = :cycleNo");
            query.setParameter("cycleNo",cycleNo);
               
            List<Bicycle> b=query.getResultList();
            if(b.size()==0)
                return null;
            else 
                return b.get(0);
            
        } finally {
            em.close();
        }
    }

    public Bicycle findBicycle(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Bicycle.class, id);
        } finally {
            em.close();
        }
    }

    public int getBicycleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Bicycle> rt = cq.from(Bicycle.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
