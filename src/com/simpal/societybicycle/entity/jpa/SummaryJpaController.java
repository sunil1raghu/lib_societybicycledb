/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import com.simpal.societybicycle.entity.Summary;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author SunilRaghuvanshi
 */
public class SummaryJpaController {
    
    public SummaryJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
     public List<Summary> findSummaryByCustomerZsn(String zsn,long fromTime,long toTime) throws Exception {
        EntityManager em = getEntityManager();
        try {
            Query query =null;
            SimpleDateFormat fr= new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            SimpleDateFormat to=new SimpleDateFormat("yyyy-MM-dd 23:59:59");
            SimpleDateFormat ac=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            
            if(toTime==0){
                    query=em.createQuery("SELECT s FROM Summary s WHERE s.customerZsn = :customerZsn "
                            + "and s.issueTime > :fromDate and s.issueTime < :toDate");
                    query.setParameter("customerZsn", zsn);
                    query.setParameter("fromDate",ac.parse(fr.format(new Date(fromTime))));
                    query.setParameter("toDate",ac.parse(to.format(new Date(fromTime))));
            }else{
            query=
                em.createQuery("SELECT s FROM Summary s WHERE s.customerZsn = :customerZsn "
                        + "and s.issueTime > :fromDate and s.issueTime < :toDate");
                query.setParameter("customerZsn", zsn);
                query.setParameter("fromDate",ac.parse(fr.format(new Date(fromTime))));
                    query.setParameter("toDate",ac.parse(to.format(new Date(toTime))));
            }
            List<Summary> summary=query.getResultList();

            if(summary.size()>0)
                return summary;
            else
                return null;



            
            
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
     public List<Summary> findSummaryByMerchantId(String merchantId,long fromTime,long toTime) throws Exception {
        EntityManager em = getEntityManager();
        try {
            Query query =null;
            SimpleDateFormat fr= new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            SimpleDateFormat to=new SimpleDateFormat("yyyy-MM-dd 23:59:59");
            SimpleDateFormat ac=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            if(toTime==0){
                    query=em.createQuery("SELECT s FROM Summary s WHERE s.merchantId = :merchantId "
                            + "and s.issueTime > :fromDate and s.issueTime < :toDate");
                    query.setParameter("merchantId", new Integer(merchantId));
                    query.setParameter("fromDate",ac.parse(fr.format(new Date(fromTime))));
                    query.setParameter("toDate",ac.parse(to.format(new Date(fromTime))));
            }else{
            query=
                em.createQuery("SELECT s FROM Summary s WHERE s.merchantId = :merchantId "
                        + "and s.issueTime > :fromDate and s.issueTime < :toDate");
                query.setParameter("merchantId", new Integer(merchantId));
                query.setParameter("fromDate",ac.parse(fr.format(new Date(fromTime))));
                    query.setParameter("toDate",ac.parse(to.format(new Date(toTime))));
            }
            List<Summary> summary=query.getResultList();

            if(summary.size()>0)
                return summary;
            else
                return null;



            
            
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
     
      public List<Summary> findSummaryByOperatorZsn(String zsn,long fromTime,long toTime) throws Exception {
        EntityManager em = getEntityManager();
        try {
            Query query =null;
            SimpleDateFormat fr= new SimpleDateFormat("yyyy-MM-dd 00:00:00");
            SimpleDateFormat to=new SimpleDateFormat("yyyy-MM-dd 23:59:59");
            SimpleDateFormat ac=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            if(toTime==0){
                    query=em.createQuery("SELECT s FROM Summary s WHERE s.oprZsn = :oprZsn "
                            + "and s.issueTime > :fromDate and s.issueTime < :toDate");
                    query.setParameter("oprZsn", zsn);
                    query.setParameter("fromDate",ac.parse(fr.format(new Date(fromTime))));
                    query.setParameter("toDate",ac.parse(to.format(new Date(fromTime))));
            }else{
            query=
                em.createQuery("SELECT s FROM Summary s WHERE s.oprZsn = :oprZsn "
                        + "and s.issueTime > :fromDate and s.issueTime < :toDate");
                query.setParameter("oprZsn", zsn);
                query.setParameter("fromDate",ac.parse(fr.format(new Date(fromTime))));
                    query.setParameter("toDate",ac.parse(to.format(new Date(toTime))));
            }
            List<Summary> summary=query.getResultList();

            if(summary.size()>0)
                return summary;
            else
                return null;



            
            
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
}
