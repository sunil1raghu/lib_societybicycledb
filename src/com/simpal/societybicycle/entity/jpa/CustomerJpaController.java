/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.jpa;

import com.simpal.societybicycle.entity.Customer;
import com.simpal.societybicycle.entity.Customer_;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.simpal.societybicycle.entity.EkycInfo;
import com.simpal.societybicycle.entity.Operator;
import com.simpal.societybicycle.entity.jpa.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;

/**
 *
 * @author SunilRaghuvanshi
 */
public class CustomerJpaController implements Serializable {

    public CustomerJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Customer customer) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            EkycInfo ekycId = customer.getEkycId();
            if (ekycId != null) {
                ekycId = em.getReference(ekycId.getClass(), ekycId.getEkycId());
                customer.setEkycId(ekycId);
            }
            em.persist(customer);
            if (ekycId != null) {
                ekycId.getCustomerList().add(customer);
                ekycId = em.merge(ekycId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Customer customer) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer persistentCustomer = em.find(Customer.class, customer.getId());
            EkycInfo ekycIdOld = persistentCustomer.getEkycId();
            EkycInfo ekycIdNew = customer.getEkycId();
            if (ekycIdNew != null) {
                ekycIdNew = em.getReference(ekycIdNew.getClass(), ekycIdNew.getEkycId());
                customer.setEkycId(ekycIdNew);
            }
            customer = em.merge(customer);
            if (ekycIdOld != null && !ekycIdOld.equals(ekycIdNew)) {
                ekycIdOld.getCustomerList().remove(customer);
                ekycIdOld = em.merge(ekycIdOld);
            }
            if (ekycIdNew != null && !ekycIdNew.equals(ekycIdOld)) {
                ekycIdNew.getCustomerList().add(customer);
                ekycIdNew = em.merge(ekycIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = customer.getId();
                if (findCustomer(id) == null) {
                    throw new NonexistentEntityException("The customer with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Customer customer;
            try {
                customer = em.getReference(Customer.class, id);
                customer.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The customer with id " + id + " no longer exists.", enfe);
            }
            EkycInfo ekycId = customer.getEkycId();
            if (ekycId != null) {
                ekycId.getCustomerList().remove(customer);
                ekycId = em.merge(ekycId);
            }
            em.remove(customer);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Customer> findCustomerEntities() {
        return findCustomerEntities(true, -1, -1);
    }

    public List<Customer> findCustomerEntities(int maxResults, int firstResult) {
        return findCustomerEntities(false, maxResults, firstResult);
    }

    private List<Customer> findCustomerEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Customer.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
        public Customer findCustomerByAadhaar(String aadhaar) throws Exception {
               EntityManager em = getEntityManager();
               Customer op=null;
               try {


                   CriteriaBuilder builder=em.getCriteriaBuilder();
                   CriteriaQuery cq = builder.createQuery();
                   Root<Customer> rt = cq.from(Customer.class);
                   
                   Predicate aadharExp=builder.equal(rt.get(Customer_.aadharNumber), aadhaar);
                   Predicate ActiveExp=builder.equal(rt.get(Customer_.status), "A");
                   Predicate and=builder.and(aadharExp,ActiveExp);
                   cq.where(and);
                   List<Customer> custList=em.createQuery(cq).getResultList();
                   if(custList.size()>0)
                       return custList.get(0);
                   else
                       return null;
               } catch (Exception ex) {
                   String msg = ex.getLocalizedMessage();
                   throw ex;
               } finally {
                   if (em != null) {
                       em.close();
                   }
               }
        }
        
        public Customer findCustomerByZsn(String zsn) throws Exception {
               EntityManager em = getEntityManager();
               Customer op=null;
               try {
                   CriteriaBuilder builder=em.getCriteriaBuilder();
                   CriteriaQuery cq = builder.createQuery();
                   Root<Customer> rt = cq.from(Customer.class);
                   
                   Predicate aadharExp=builder.equal(rt.get(Customer_.zsn), zsn);
                   Predicate ActiveExp=builder.equal(rt.get(Customer_.status), "A");
                   Predicate and=builder.and(aadharExp,ActiveExp);
                   cq.where(and);
                   List<Customer> custList=em.createQuery(cq).getResultList();
                   if(custList.size()>0)
                       return custList.get(0);
                   else
                       return null;
               } catch (Exception ex) {
                   String msg = ex.getLocalizedMessage();
                   throw ex;
               } finally {
                   if (em != null) {
                       em.close();
                   }
               }
        }
    
    
    public Customer findCustomer(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Customer.class, id);
        } finally {
            em.close();
        }
    }

    public int getCustomerCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Customer> rt = cq.from(Customer.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
