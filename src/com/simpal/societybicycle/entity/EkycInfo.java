/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SunilRaghuvanshi
 */
@Entity
@Table(name = "ekyc_info", catalog = "society_cycle", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EkycInfo.findAll", query = "SELECT e FROM EkycInfo e"),
    @NamedQuery(name = "EkycInfo.findByEkycId", query = "SELECT e FROM EkycInfo e WHERE e.ekycId = :ekycId"),
    @NamedQuery(name = "EkycInfo.findByEmail", query = "SELECT e FROM EkycInfo e WHERE e.email = :email"),
    @NamedQuery(name = "EkycInfo.findByDob", query = "SELECT e FROM EkycInfo e WHERE e.dob = :dob"),
    @NamedQuery(name = "EkycInfo.findByAadharMobile", query = "SELECT e FROM EkycInfo e WHERE e.aadharMobile = :aadharMobile"),
    @NamedQuery(name = "EkycInfo.findByGender", query = "SELECT e FROM EkycInfo e WHERE e.gender = :gender"),
    @NamedQuery(name = "EkycInfo.findByDistrict", query = "SELECT e FROM EkycInfo e WHERE e.district = :district"),
    @NamedQuery(name = "EkycInfo.findBySubDistrict", query = "SELECT e FROM EkycInfo e WHERE e.subDistrict = :subDistrict"),
    @NamedQuery(name = "EkycInfo.findByStates", query = "SELECT e FROM EkycInfo e WHERE e.states = :states"),
    @NamedQuery(name = "EkycInfo.findByPinCode", query = "SELECT e FROM EkycInfo e WHERE e.pinCode = :pinCode"),
    @NamedQuery(name = "EkycInfo.findByPostOffice", query = "SELECT e FROM EkycInfo e WHERE e.postOffice = :postOffice")})
public class EkycInfo implements Serializable {

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ekyc_id", nullable = false)
    private Integer ekycId;
    @Column(name = "email", length = 50)
    private String email;
    @Column(name = "dob")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Column(name = "aadhar_mobile", length = 10)
    private String aadharMobile;
    @Column(name = "gender")
    private Character gender;
    @Column(name = "district", length = 50)
    private String district;
    @Column(name = "sub_district", length = 50)
    private String subDistrict;
    @Column(name = "states", length = 50)
    private String states;
    @Column(name = "pin_code", length = 6)
    private String pinCode;
    @Column(name = "post_office", length = 50)
    private String postOffice;
    @OneToMany(mappedBy = "ekycId", fetch = FetchType.EAGER)
    private List<Customer> customerList;

    public EkycInfo() {
    }

    public EkycInfo(Integer ekycId) {
        this.ekycId = ekycId;
    }

    public Integer getEkycId() {
        return ekycId;
    }

    public void setEkycId(Integer ekycId) {
        this.ekycId = ekycId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAadharMobile() {
        return aadharMobile;
    }

    public void setAadharMobile(String aadharMobile) {
        this.aadharMobile = aadharMobile;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getSubDistrict() {
        return subDistrict;
    }

    public void setSubDistrict(String subDistrict) {
        this.subDistrict = subDistrict;
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getPostOffice() {
        return postOffice;
    }

    public void setPostOffice(String postOffice) {
        this.postOffice = postOffice;
    }

    @XmlTransient
    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ekycId != null ? ekycId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EkycInfo)) {
            return false;
        }
        EkycInfo other = (EkycInfo) object;
        if ((this.ekycId == null && other.ekycId != null) || (this.ekycId != null && !this.ekycId.equals(other.ekycId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.simpal.societybicycle.entity.EkycInfo[ ekycId=" + ekycId + " ]";
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }
    
}
