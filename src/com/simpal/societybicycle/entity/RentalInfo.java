/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SunilRaghuvanshi
 */
@Entity
@Table(name = "rental_info", catalog = "society_cycle", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RentalInfo.findAll", query = "SELECT r FROM RentalInfo r"),
    @NamedQuery(name = "RentalInfo.findById", query = "SELECT r FROM RentalInfo r WHERE r.id = :id"),
    @NamedQuery(name = "RentalInfo.findByTxnId", query = "SELECT r FROM RentalInfo r WHERE r.txnId = :txnId"),
    @NamedQuery(name = "RentalInfo.findByCycleNo", query = "SELECT r FROM RentalInfo r WHERE r.cycleNo = :cycleNo"),
    @NamedQuery(name = "RentalInfo.findByCustomerZsn", query = "SELECT r FROM RentalInfo r WHERE r.customerZsn = :customerZsn"),
    @NamedQuery(name = "RentalInfo.findByIssueTime", query = "SELECT r FROM RentalInfo r WHERE r.issueTime = :issueTime"),
    @NamedQuery(name = "RentalInfo.findByDepositTime", query = "SELECT r FROM RentalInfo r WHERE r.depositTime = :depositTime")})
public class RentalInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "txn_id", length = 20)
    private String txnId;
    @Column(name = "cycle_no", length = 15)
    private String cycleNo;
    @Column(name = "customer_zsn", length = 9)
    private String customerZsn;
    @Column(name = "issue_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date issueTime;
    @Column(name = "deposit_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date depositTime;
    @JoinColumn(name = "issue_device_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Device issueDeviceId;
    @JoinColumn(name = "deposit_device_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Device depositDeviceId;
    @JoinColumn(name = "issue_operator_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Operator issueOperatorId;
    @JoinColumn(name = "deposit_operator_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Operator depositOperatorId;

    public RentalInfo() {
    }

    public RentalInfo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getCycleNo() {
        return cycleNo;
    }

    public void setCycleNo(String cycleNo) {
        this.cycleNo = cycleNo;
    }

    public String getCustomerZsn() {
        return customerZsn;
    }

    public void setCustomerZsn(String customerZsn) {
        this.customerZsn = customerZsn;
    }

    public Date getIssueTime() {
        return issueTime;
    }

    public void setIssueTime(Date issueTime) {
        this.issueTime = issueTime;
    }

    public Date getDepositTime() {
        return depositTime;
    }

    public void setDepositTime(Date depositTime) {
        this.depositTime = depositTime;
    }

    public Device getIssueDeviceId() {
        return issueDeviceId;
    }

    public void setIssueDeviceId(Device issueDeviceId) {
        this.issueDeviceId = issueDeviceId;
    }

    public Device getDepositDeviceId() {
        return depositDeviceId;
    }

    public void setDepositDeviceId(Device depositDeviceId) {
        this.depositDeviceId = depositDeviceId;
    }

    public Operator getIssueOperatorId() {
        return issueOperatorId;
    }

    public void setIssueOperatorId(Operator issueOperatorId) {
        this.issueOperatorId = issueOperatorId;
    }

    public Operator getDepositOperatorId() {
        return depositOperatorId;
    }

    public void setDepositOperatorId(Operator depositOperatorId) {
        this.depositOperatorId = depositOperatorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RentalInfo)) {
            return false;
        }
        RentalInfo other = (RentalInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.simpal.societybicycle.entity.RentalInfo[ id=" + id + " ]";
    }
    
}
