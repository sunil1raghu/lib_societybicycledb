/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SunilRaghuvanshi
 */
@Entity
@Table(name = "customer", catalog = "society_cycle", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"zsn"}),
    @UniqueConstraint(columnNames = {"card_uid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
    @NamedQuery(name = "Customer.findById", query = "SELECT c FROM Customer c WHERE c.id = :id"),
    @NamedQuery(name = "Customer.findByOperatorId", query = "SELECT c FROM Customer c WHERE c.operatorId = :operatorId"),
    @NamedQuery(name = "Customer.findByStatus", query = "SELECT c FROM Customer c WHERE c.status = :status"),
    @NamedQuery(name = "Customer.findByTxnRefNumber", query = "SELECT c FROM Customer c WHERE c.txnRefNumber = :txnRefNumber"),
    @NamedQuery(name = "Customer.findByDeviceImei", query = "SELECT c FROM Customer c WHERE c.deviceImei = :deviceImei"),
    @NamedQuery(name = "Customer.findByZsn", query = "SELECT c FROM Customer c WHERE c.zsn = :zsn"),
    @NamedQuery(name = "Customer.findByCardUid", query = "SELECT c FROM Customer c WHERE c.cardUid = :cardUid"),
    @NamedQuery(name = "Customer.findByCustMobile", query = "SELECT c FROM Customer c WHERE c.custMobile = :custMobile"),
    @NamedQuery(name = "Customer.findByAadharNumber", query = "SELECT c FROM Customer c WHERE c.aadharNumber = :aadharNumber"),
    @NamedQuery(name = "Customer.findByCustName", query = "SELECT c FROM Customer c WHERE c.custName = :custName"),
    @NamedQuery(name = "Customer.findBySmsFlag", query = "SELECT c FROM Customer c WHERE c.smsFlag = :smsFlag"),
    @NamedQuery(name = "Customer.findByPreferredLanguage", query = "SELECT c FROM Customer c WHERE c.preferredLanguage = :preferredLanguage"),
    @NamedQuery(name = "Customer.findByMobileType", query = "SELECT c FROM Customer c WHERE c.mobileType = :mobileType"),
    @NamedQuery(name = "Customer.findByIssuenceFlag", query = "SELECT c FROM Customer c WHERE c.issuenceFlag = :issuenceFlag"),
    @NamedQuery(name = "Customer.findByDeviceTimestamp", query = "SELECT c FROM Customer c WHERE c.deviceTimestamp = :deviceTimestamp"),
    @NamedQuery(name = "Customer.findByServerTimestamp", query = "SELECT c FROM Customer c WHERE c.serverTimestamp = :serverTimestamp"),
    @NamedQuery(name = "Customer.findByCardBalance", query = "SELECT c FROM Customer c WHERE c.cardBalance = :cardBalance"),
    @NamedQuery(name = "Customer.findByServerBalance", query = "SELECT c FROM Customer c WHERE c.serverBalance = :serverBalance"),
    @NamedQuery(name = "Customer.findByCustomerType", query = "SELECT c FROM Customer c WHERE c.customerType = :customerType")})
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "operator_id")
    private Integer operatorId;
    @Column(name = "status", length = 2)
    private String status;
    @Column(name = "txn_ref_number", length = 20)
    private String txnRefNumber;
    @Column(name = "device_imei", length = 15)
    private String deviceImei;
    @Column(name = "zsn", length = 9)
    private String zsn;
    @Column(name = "card_uid", length = 14)
    private String cardUid;
    @Column(name = "cust_mobile", length = 10)
    private String custMobile;
    @Column(name = "aadhar_number", length = 12)
    private String aadharNumber;
    @Column(name = "cust_name", length = 40)
    private String custName;
    @Column(name = "sms_flag")
    private Character smsFlag;
    @Column(name = "preferred_language")
    private Character preferredLanguage;
    @Column(name = "mobile_type")
    private Character mobileType;
    @Column(name = "issuence_flag", length = 2)
    private String issuenceFlag;
    @Column(name = "device_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deviceTimestamp;
    @Column(name = "server_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date serverTimestamp;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "card_balance", precision = 17, scale = 17)
    private Double cardBalance;
    @Column(name = "server_balance", precision = 17, scale = 17)
    private Double serverBalance;
    @Column(name = "customer_type", length = 2)
    private String customerType;
    @JoinColumn(name = "ekyc_id", referencedColumnName = "ekyc_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private EkycInfo ekycId;

    public Customer() {
    }

    public Customer(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTxnRefNumber() {
        return txnRefNumber;
    }

    public void setTxnRefNumber(String txnRefNumber) {
        this.txnRefNumber = txnRefNumber;
    }

    public String getDeviceImei() {
        return deviceImei;
    }

    public void setDeviceImei(String deviceImei) {
        this.deviceImei = deviceImei;
    }

    public String getZsn() {
        return zsn;
    }

    public void setZsn(String zsn) {
        this.zsn = zsn;
    }

    public String getCardUid() {
        return cardUid;
    }

    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Character getSmsFlag() {
        return smsFlag;
    }

    public void setSmsFlag(Character smsFlag) {
        this.smsFlag = smsFlag;
    }

    public Character getPreferredLanguage() {
        return preferredLanguage;
    }

    public void setPreferredLanguage(Character preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    public Character getMobileType() {
        return mobileType;
    }

    public void setMobileType(Character mobileType) {
        this.mobileType = mobileType;
    }

    public String getIssuenceFlag() {
        return issuenceFlag;
    }

    public void setIssuenceFlag(String issuenceFlag) {
        this.issuenceFlag = issuenceFlag;
    }

    public Date getDeviceTimestamp() {
        return deviceTimestamp;
    }

    public void setDeviceTimestamp(Date deviceTimestamp) {
        this.deviceTimestamp = deviceTimestamp;
    }

    public Date getServerTimestamp() {
        return serverTimestamp;
    }

    public void setServerTimestamp(Date serverTimestamp) {
        this.serverTimestamp = serverTimestamp;
    }

    public Double getCardBalance() {
        return cardBalance;
    }

    public void setCardBalance(Double cardBalance) {
        this.cardBalance = cardBalance;
    }

    public Double getServerBalance() {
        return serverBalance;
    }

    public void setServerBalance(Double serverBalance) {
        this.serverBalance = serverBalance;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public EkycInfo getEkycId() {
        return ekycId;
    }

    public void setEkycId(EkycInfo ekycId) {
        this.ekycId = ekycId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.simpal.societybicycle.entity.Customer[ id=" + id + " ]";
    }
    
}
