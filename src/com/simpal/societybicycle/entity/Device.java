/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author SunilRaghuvanshi
 */
@Entity
@Table(name = "device", catalog = "society_cycle", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Device.findAll", query = "SELECT d FROM Device d"),
    @NamedQuery(name = "Device.findById", query = "SELECT d FROM Device d WHERE d.id = :id"),
    @NamedQuery(name = "Device.findByImei", query = "SELECT d FROM Device d WHERE d.imei = :imei"),
    @NamedQuery(name = "Device.findByStatus", query = "SELECT d FROM Device d WHERE d.status = :status")})
public class Device implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "imei", length = 15)
    private String imei;
    @Column(name = "status", length = 2)
    private String status;
    @OneToMany(mappedBy = "issueDeviceId", fetch = FetchType.EAGER)
    private List<RentalInfo> rentalInfoList;
    @OneToMany(mappedBy = "depositDeviceId", fetch = FetchType.EAGER)
    private List<RentalInfo> rentalInfoList1;
    @JoinColumn(name = "merchant_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Merchant merchantId;

    public Device() {
    }

    public Device(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public List<RentalInfo> getRentalInfoList() {
        return rentalInfoList;
    }

    public void setRentalInfoList(List<RentalInfo> rentalInfoList) {
        this.rentalInfoList = rentalInfoList;
    }

    @XmlTransient
    public List<RentalInfo> getRentalInfoList1() {
        return rentalInfoList1;
    }

    public void setRentalInfoList1(List<RentalInfo> rentalInfoList1) {
        this.rentalInfoList1 = rentalInfoList1;
    }

    public Merchant getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchant merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Device)) {
            return false;
        }
        Device other = (Device) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.simpal.societybicycle.entity.Device[ id=" + id + " ]";
    }
    
}
