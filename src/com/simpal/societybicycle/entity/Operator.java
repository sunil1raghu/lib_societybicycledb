/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author SunilRaghuvanshi
 */
@Entity
@Table(name = "operator", catalog = "society_cycle", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"zsn"}),
    @UniqueConstraint(columnNames = {"card_uid"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operator.findAll", query = "SELECT o FROM Operator o"),
    @NamedQuery(name = "Operator.findById", query = "SELECT o FROM Operator o WHERE o.id = :id"),
    @NamedQuery(name = "Operator.findByOprName", query = "SELECT o FROM Operator o WHERE o.oprName = :oprName"),
    @NamedQuery(name = "Operator.findByZsn", query = "SELECT o FROM Operator o WHERE o.zsn = :zsn"),
    @NamedQuery(name = "Operator.findByCardUid", query = "SELECT o FROM Operator o WHERE o.cardUid = :cardUid"),
    @NamedQuery(name = "Operator.findByMobile", query = "SELECT o FROM Operator o WHERE o.mobile = :mobile"),
    @NamedQuery(name = "Operator.findByEmail", query = "SELECT o FROM Operator o WHERE o.email = :email"),
    @NamedQuery(name = "Operator.findByAddress", query = "SELECT o FROM Operator o WHERE o.address = :address"),
    @NamedQuery(name = "Operator.findByStatus", query = "SELECT o FROM Operator o WHERE o.status = :status"),
    @NamedQuery(name = "Operator.findByPin", query = "SELECT o FROM Operator o WHERE o.pin = :pin")})
public class Operator implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "opr_name", length = 40)
    private String oprName;
    @Column(name = "zsn", length = 9)
    private String zsn;
    @Column(name = "card_uid", length = 14)
    private String cardUid;
    @Column(name = "mobile", length = 10)
    private String mobile;
    @Column(name = "email", length = 50)
    private String email;
    @Column(name = "address", length = 200)
    private String address;
    @Column(name = "status", length = 2)
    private String status;
    @Column(name = "pin", length = 50)
    private String pin;
    @JoinColumn(name = "merchant_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Merchant merchantId;

    public Operator() {
    }

    public Operator(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOprName() {
        return oprName;
    }

    public void setOprName(String oprName) {
        this.oprName = oprName;
    }

    public String getZsn() {
        return zsn;
    }

    public void setZsn(String zsn) {
        this.zsn = zsn;
    }

    public String getCardUid() {
        return cardUid;
    }

    public void setCardUid(String cardUid) {
        this.cardUid = cardUid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public Merchant getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Merchant merchantId) {
        this.merchantId = merchantId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operator)) {
            return false;
        }
        Operator other = (Operator) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.simpal.societybicycle.entity.Operator[ id=" + id + " ]";
    }
    
}
