/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simpal.societybicycle.entity.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author SunilRaghuvanshi
 */
public class SocietyDBEntityFactory {

    private static EntityManagerFactory entityManagerFactory;
    private static final int test=123;
    private static final int production=1354;
    
    public static EntityManagerFactory getEntityManagerFactory(int setup)
    {
        if(entityManagerFactory==null)
        {
            if(setup==test)
                entityManagerFactory=Persistence.createEntityManagerFactory("Lib_SocietyBicycleDBPU");
            else if(setup==production)
                entityManagerFactory=Persistence.createEntityManagerFactory("Lib_SocietyBicycleDBPU_Production");
        }
        return entityManagerFactory;
    }
    
    
    
    
}
